<?php

class bracket
{
    const BRACKETS = [
        '(' => ')',
        '[' => ']',
        '{' => '}'
    ];

    /**
     * На входе принимает строку из скобок, и возвращает true если все открытые скобки закрыты, иначе - false.
     * Recursive function for check to couples brackets
     * @param string $bracketSet
     * @return bool
     * @example Варианты скобок: ()[]{}
     */
    public static function check(string $bracketSet): bool
    {
        if (strlen($bracketSet) < 2) {
            return false;
        }

        if (strlen($bracketSet) == 2) {
            return self::BRACKETS[$bracketSet[0]] === $bracketSet[1];
        }

        if (in_array($bracketSet[0],self::BRACKETS) ||
            in_array($bracketSet[strlen($bracketSet) - 1], array_keys(self::BRACKETS))
        ) {
            return false;
        }

        $symbol = $bracketSet[0];
        $oppositeSymbolPosition = strpos($bracketSet, self::BRACKETS[$symbol], 1);

        if ($oppositeSymbolPosition === false) {
            return false;

        } else {
            $bracketSubSet = bracket::cut($bracketSet, $oppositeSymbolPosition);
            $bracketSubSet = substr($bracketSubSet, 1, strlen($bracketSubSet) - 1);
            return self::check($bracketSubSet);
        }
    }

    /**
     * Cut symbol from string
     * @param string $string
     * @param int $symbolPosition
     * @return string
     */
    private static function cut(string $string, int $symbolPosition): string
    {
        if ($symbolPosition === 0) {
            return substr($string, 1, strlen($string) - 1);
        }

        if ($symbolPosition < 0 || $symbolPosition >= strlen($string)) {
            return $string;
        }

        return substr($string, 0, $symbolPosition) . substr($string, $symbolPosition + 1, strlen($string));
    }
}

if (isset($_POST['inputBrackets'])) {

    $inputBrackets = htmlspecialchars($_POST['inputBrackets'], ENT_QUOTES, 'UTF-8');
    $result = bracket::check($inputBrackets) ? 'true' : 'false';

    $host  = $_SERVER['HTTP_HOST'];
    $uri   = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
    $extra = "bracket.php?result={$result}&input={$inputBrackets}";

    header("Location: http://$host$uri/$extra");
    exit;
}

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Check Brackets</title>
    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
</head>
<body>
<!-- Begin page content -->
<main role="main" class="container">

    <div class="pt-3">
        <?php if (isset($_GET['result']) && in_array($_GET['result'], ['true', 'false'])): ?>
            <?php if ($_GET['result'] === 'true'): ?>
                <div class="alert alert-success" role="alert">
                    True
                </div>
            <?php endif ?>
            <?php if ($_GET['result'] === 'false'): ?>
                <div class="alert alert-warning" role="alert">
                    False
                </div>
            <?php endif ?>
        <?php endif ?>
    </div>

    <h1 class="mt-5">Check Brackets</h1>
    <form method="post">
        <div class="form-group">
            <label for="exampleInputEmail1">Brackets</label>
            <input type="text" class="form-control" name="inputBrackets" placeholder="[({})](]" value="<?= $_GET['input'] ?? '' ?>" required>
            <small class="form-text text-muted">Варианты скобок: ()[]{}</small>
        </div>
        <button type="submit" class="btn btn-primary">Проверить</button>
    </form>

</main>
</body>
</html>
