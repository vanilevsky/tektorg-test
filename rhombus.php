<?php

class rhombus
{
    const DIRECTION_BOTTOM = 'bottom';
    const DIRECTION_TOP = 'top';

    const SYMBOL_ASTERISK = '*';
    const SYMBOL_SPACE = ' ';

    /**
     * Принимаем в качестве аргумента количество строк в итоговом массиве
     * Нужно вписать в массив ромб из символов *​, если позволяет размерность, иначе - заполнять до конца в виде башни
     * @param int $dimension
     * @return string
     */
    public static function build(int $dimension): string
    {
        if ($dimension < 1) {
            return self::SYMBOL_SPACE;
        }

        if ($dimension === 1) {
            return self::SYMBOL_ASTERISK . PHP_EOL . PHP_EOL. PHP_EOL;
        }

        /** @var string $data */
        $data = '';

        $direction = self::DIRECTION_BOTTOM;

        for ($line = 1, $step = 1; $line <= $dimension; $line++) {

            $data .= self::spaces($dimension, $step);
            $data .= str_repeat(self::SYMBOL_ASTERISK, $step);
            $data .= self::spaces($dimension, $step);

            if ($direction === self::DIRECTION_BOTTOM) {
                $step += 2;
            } else {
                $step -= 2;
            }

            if ($step >= $dimension && ($dimension % 2 !== 0)) {
                $direction = self::DIRECTION_TOP;
            }

            $data .= PHP_EOL;
        }

        return $data . PHP_EOL . PHP_EOL;
    }

    /**
     * @param string $dimension
     * @param string $step
     * @return string
     */
    private static function spaces(string $dimension, string $step): string
    {
        if ($dimension % 2 !== 0) {
            return $dimension - $step > 1 ? str_repeat(self::SYMBOL_SPACE, ($dimension - $step) / 2) : '';
        } else {
            return $dimension * 2 - $step > 1 ? str_repeat(self::SYMBOL_SPACE, ($dimension * 2 - $step - 1) / 2) : '';
        }
    }
}
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Rhombus</title>
    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
</head>
<body>
<!-- Begin page content -->
<main role="main" class="container">

    <h1 class="mt-5">Rhombus</h1>

    <div>
        <pre>
<?php for ($dimension = 1; $dimension < 30; $dimension++): ?>
<?= rhombus::build($dimension) ?>
<?php endfor ?>
        </pre>
    </div>

</main>
</body>
</html>


