CREATE TABLE `author` (
    `id` int AUTO_INCREMENT,
    `name` varchar(255),
    PRIMARY KEY (`id`)
);

CREATE TABLE `book` (
    `id` int AUTO_INCREMENT,
    `title` varchar(255),
    `author_id` int,
    `rating` decimal(3, 1),
    PRIMARY KEY (`id`)
);

CREATE TABLE `book_sale` (
    `id` int AUTO_INCREMENT,
    `book_id` int,
    PRIMARY KEY (`id`)
);

INSERT INTO author (id, name) VALUES
    (1, 'Лев Толстой'),
    (2, 'Александр Пушкин'),
    (3, 'Михаил Лермонтов');

INSERT INTO book (id, title, author_id, rating) VALUES
    (1, 'Война и мир', 1, 3),
    (2, 'Анна Каренина', 1, 4),
    (3, 'Воскресение', 1, 2),
    (4, 'Руслан и Людмила', 2, 4),
    (5, 'Цыганы', 2, 2),
    (6, 'Евгений Онегин', 2, 5),
    (7, 'Медный всадник', 2, 4),
    (8, 'Кавказский пленник', 2, 4),
    (9, 'Бородино', 3, 5),
    (10, 'Герой нашего времени', 3, 4),
    (11, 'Дума', 3, 4),
    (12, 'Парус', 3, 2),
    (13, 'Ветка Палестины', 3, 1);

INSERT INTO book_sale (id, book_id) VALUES
    (1, 1),
    (2, 1),
    (3, 1),
    (4, 1),
    (5, 2),
    (6, 2),
    (7, 2),
    (8, 2),
    (9, 3),
    (10, 3),
    (11, 4),
    (12, 4),
    (13, 4),
    (14, 4),
    (15, 4),
    (16, 10),
    (17, 10),
    (18, 10),
    (19, 10),
    (20, 10);