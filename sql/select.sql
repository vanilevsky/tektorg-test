-- Написать SQL запрос. Есть таблицы author, book и book_sale. Последняя содержит
-- данные по проданным книгам (если книга продана 7 раз, то будет 7 записей в таблице).
-- Нужно вывести имя автора, количество проданных книг, средний рейтинг проданных
-- книг. При этом выводить, только если количество проданных книг более 3, и их средний
-- рейтинг больше 3 (рейтинг может быть в диапазоне от 1 до 5). Результаты нужно
-- отсортировать по количеству проданных книг от большего к меньшему.

select  a1.name as author_id, count(a3.id) as count_sale, round(avg(rating), 2) as avg_rating
from author a1
    join book a2 on a2.author_id = a1.id
    left join book_sale a3 on a2.id = a3.book_id
group by a1.name having(count(*) > 3 and avg(rating) > 3)
order by count(a3.id) desc